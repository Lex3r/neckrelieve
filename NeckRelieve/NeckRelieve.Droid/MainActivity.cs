﻿using Android.OS;
using Android.App;
using Android.Views;
using Android.Gms.Ads;
using Android.Content;
using Android.Content.PM;
using RoundedBoxView.Forms.Plugin.Droid;
using Plugin.LocalNotifications;
using Plugin.InAppBilling;
using NeckRelieve.Droid.Renderers;



namespace NeckRelieve.Droid
{
    [Activity(Label = "Neck Relieve", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = false, ScreenOrientation = ScreenOrientation.Portrait, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        protected override void OnCreate(Bundle bundle)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);

            Window.SetFlags(WindowManagerFlags.KeepScreenOn, WindowManagerFlags.KeepScreenOn);

            global::Xamarin.Forms.Forms.Init(this, bundle);
            RoundedBoxViewRenderer.Init();
            GifImageViewRenderer.Init();
            CircularProgressRenderer.Init();

            MobileAds.Initialize(ApplicationContext, "ca-app-pub-7393334420865995~5514116261");

            LocalNotificationsImplementation.NotificationIconId = Resource.Drawable.reminder;

            LoadApplication(new App());
        }

        protected override void OnActivityResult(int requestCode, Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            InAppBillingImplementation.HandleActivityResult(requestCode, resultCode, data);
        }
    }
}

