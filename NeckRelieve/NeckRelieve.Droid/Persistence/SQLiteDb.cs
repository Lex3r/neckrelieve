using System;
using System.IO;
using SQLite;
using Xamarin.Forms;
using NeckRelieve.Persistence;
using NeckRelieve.Droid.Persistence;

[assembly: Dependency(typeof(SQLiteDb))]

namespace NeckRelieve.Droid.Persistence
{
    public class SQLiteDb : ISQLiteDb
    {
        public SQLiteConnection GetConnection()
        {
            var documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            var path = Path.Combine(documentsPath, "user.db3");

            return new SQLiteConnection(path);
        }
    }
}