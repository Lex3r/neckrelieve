using Xamarin.Forms;
using Android.Media;
using NeckRelieve.Services;
using NeckRelieve.Droid.Services;


[assembly: Dependency(typeof(AudioService))]

namespace NeckRelieve.Droid.Services
{
    public class AudioService : IAudio
    {
        private MediaPlayer _mediaPlayer;


        public void PlayBeep()
        {
            _mediaPlayer = MediaPlayer.Create(Android.App.Application.Context, Resource.Raw.beep);
            _mediaPlayer.Start();
        }

        public void PlayApplause()
        {
            _mediaPlayer = MediaPlayer.Create(Android.App.Application.Context, Resource.Raw.applause);
            _mediaPlayer.Start();
        }
    }
}