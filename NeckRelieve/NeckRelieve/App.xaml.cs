﻿using Xamarin.Forms;
using NeckRelieve.Views;
using NeckRelieve.Helpers;


namespace NeckRelieve
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            var navigationPage = new NavigationPage(new MainPage()) {
                BarBackgroundColor = Color.FromHex("#22c8da")
            };

            MainPage = navigationPage;
        }

        protected override void OnStart()
        {
            // Handle when your app starts

            new NotificationService().UpdateNotification();
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
