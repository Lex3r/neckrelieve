﻿using System;
using Plugin.Settings;
using Plugin.Settings.Abstractions;


namespace NeckRelieve.Helpers
{
    public static class Settings
    {
        private static ISettings AppSettings => CrossSettings.Current;


        private const string PreviousDateKey = "PreviousDate";

        public static DateTime PreviousDate
        {
            get
            {
                return AppSettings.GetValueOrDefault(PreviousDateKey, DateTime.Today);
            }
            set
            {
                AppSettings.AddOrUpdateValue(PreviousDateKey, value);
            }
        }


        private const string WorkoutDaysKey = "WorkoutDays";

        public static int WorkoutDays
        {
            get
            {
                return AppSettings.GetValueOrDefault(WorkoutDaysKey, 0);
            }
            set
            {
                AppSettings.AddOrUpdateValue(WorkoutDaysKey, value);
            }
        }

        private const string SoundKey = "Sound";

        public static bool Sound
        {
            get
            {
                return AppSettings.GetValueOrDefault(SoundKey, true);
            }
            set
            {
                AppSettings.AddOrUpdateValue(SoundKey, value);
            }
        }

        private const string ReminderOnKey = "ReminderOn";

        public static bool ReminderOn
        {
            get
            {
                return AppSettings.GetValueOrDefault(ReminderOnKey, false);
            }
            set
            {
                AppSettings.AddOrUpdateValue(ReminderOnKey, value);
            }
        }

        private const string ReminderTimeKey = "ReminderTime";
        public static TimeSpan ReminderTime
        {
            get
            {
                var ticks = AppSettings.GetValueOrDefault(ReminderTimeKey, new TimeSpan(11, 0, 0).Ticks);

                return new TimeSpan(ticks);
            }
            set
            {
                AppSettings.AddOrUpdateValue(ReminderTimeKey, value.Ticks);
            }
        }

        private const string DurationOfExerciseKey = "DurationOfExercise";

        public static int DurationOfExercise
        {
            get
            {
                return AppSettings.GetValueOrDefault(DurationOfExerciseKey, 60);
            }
            set
            {
                AppSettings.AddOrUpdateValue(DurationOfExerciseKey, value);
            }
        }


        private const string IsPurchasedKey = "IsPurchased";

        public static bool IsPurchased
        {
            get
            {
                return AppSettings.GetValueOrDefault(IsPurchasedKey, false);
            }
            set
            {
                AppSettings.AddOrUpdateValue(IsPurchasedKey, value);
            }
        }
    }
}
