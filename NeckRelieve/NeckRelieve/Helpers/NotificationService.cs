﻿using System;
using Plugin.LocalNotifications;


namespace NeckRelieve.Helpers
{
    public class NotificationService
    {
        public void UpdateNotification()
        {
            CrossLocalNotifications.Current.Cancel(0);

            if (Settings.ReminderOn) {
                var dateTime = DateTime.Now.TimeOfDay < Settings.ReminderTime ? DateTime.Now : DateTime.Now.AddDays(1);
                dateTime = dateTime.Date + Settings.ReminderTime;

                CrossLocalNotifications.Current.Show("Neck Pain Relief", "The Best Time to Exercise!", 0, dateTime);
            }
        }
    }
}
