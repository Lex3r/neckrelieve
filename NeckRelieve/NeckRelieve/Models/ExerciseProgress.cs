﻿using System;
using SQLite;


namespace NeckRelieve.Models
{
    public class ExerciseProgress
    {
        [PrimaryKey, AutoIncrement]
        public int? Id { get; set; }

        public DateTime Date { get; set; }

        public int Count { get; set; }
    }
}
