﻿namespace NeckRelieve.Services
{
    public interface IAudio
    {
        void PlayBeep();
        void PlayApplause();
    }
}
