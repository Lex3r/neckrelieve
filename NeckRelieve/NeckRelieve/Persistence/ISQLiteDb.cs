﻿using SQLite;

namespace NeckRelieve.Persistence
{
    public interface ISQLiteDb
    {
        SQLiteConnection GetConnection();
    }
}
