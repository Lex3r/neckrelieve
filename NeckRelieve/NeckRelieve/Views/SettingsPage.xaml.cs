﻿using System;
using System.Windows.Input;
using Xamarin.Forms;
using PropertyChanged;
using Plugin.InAppBilling;
using Plugin.InAppBilling.Abstractions;
using NeckRelieve.Helpers;


namespace NeckRelieve.Views
{
    [ImplementPropertyChanged]
    public partial class SettingsPage : ContentPage
    {

        public bool Sound
        {
            get
            {
                return Settings.Sound;
            }

            set
            {
                Settings.Sound = value;
            }
        }

        public bool ReminderOn
        {
            get
            {
                return Settings.ReminderOn;
            }

            set
            {
                Settings.ReminderOn = value;

                UpdateNotification();
            }
        }

        public TimeSpan ReminderTime
        {
            get
            {
                return Settings.ReminderTime;
            }

            set
            {
                Settings.ReminderTime = value;

                UpdateNotification();
            }
        }

        public static string DurationOfExercise
        {
            get
            {
                return Settings.DurationOfExercise.ToString();
            }
            set
            {
                Settings.DurationOfExercise = int.Parse(value);
            }
        }

        private NotificationService _notificationService;


        public ICommand RemoveAds { get; set; }
        public ICommand RateTheApp { get; set; }
        public ICommand Feedback { get; set; }



        public SettingsPage()
        {
            InitializeComponent();

            BindingContext = this;

            _notificationService = new NotificationService();

            durationPicker.SelectedIndex = durationPicker.Items.IndexOf(DurationOfExercise);
            durationPicker.SelectedIndexChanged += (s, e) => {
                DurationOfExercise = durationPicker.Items[durationPicker.SelectedIndex];
            };


            RemoveAds = new Command(RemoveAdsAction); 
            RateTheApp = new Command(RateTheAppAction);
            Feedback = new Command(FeedbackAction);
        }

        private void UpdateNotification()
        {
            _notificationService.UpdateNotification();
        }


        private async void RemoveAdsAction()
        {
            try {
                var connected = await CrossInAppBilling.Current.ConnectAsync();
                if (!connected) {
                    await DisplayAlert("Error", "Couldn't connect to billing", "OK");
                    return;
                }

                //try to purchase item
                var purchase = await CrossInAppBilling.Current.PurchaseAsync("com.axsoray.neckrelievead", ItemType.InAppPurchase, "apppayload");
                if (purchase != null) {
                    Settings.IsPurchased = true;

                    await DisplayAlert("Success", "The app successfully upgraded. Thank you.", "OK");
                } 
            } catch (Exception ex) {
                await DisplayAlert("Error", ex?.Message, "OK");                
            } finally {
                //Disconnect, it is okay if we never connected, this will never throw an exception
                await CrossInAppBilling.Current.DisconnectAsync();
            }
        }


        private void RateTheAppAction()
        {
            Device.OpenUri(new Uri($"https://play.google.com/store/apps/details?id=com.axsoray.neckrelieve"));
        }


        private void FeedbackAction()
        {
            Device.OpenUri(new Uri("mailto:axsoray@gmail.com"));
        }
        
    }
}
