﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;
using SQLite;
using PropertyChanged;
using NeckRelieve.Helpers;
using NeckRelieve.Controls;
using NeckRelieve.Services;
using NeckRelieve.Extensions;
using NeckRelieve.Persistence;
using NeckRelieve.Models;


namespace NeckRelieve.Views
{
    [ImplementPropertyChanged]
    public partial class WorkoutPage : ContentPage
    {
        private TapGestureRecognizer _startWorkoutGestureRecognizer;

        private int _exerciseIndex = 0;

        private bool _isTimerShouldRun;

        private IAudio _audioService;

        private List<int> _excercises = new List<int> { 1, 2, 3, 4 };

        private SQLiteConnection _connection;

        public bool WorkoutNotStarted { get; set; } = true;

        public bool WorkoutStarted => !WorkoutNotStarted;

        public bool ExcercisesNotDone { get; set; } = true;

        public bool ExcercisesDone => !ExcercisesNotDone;

        public int Progress { get; set; }

        public int ProgressInverse => ExerciseDuration - Progress;

        public int WorkoutDaysLabel { get; set; }

        public string CompleteText { get; set; }

        public string ExerciseText { get; set; }

        public int ExerciseDuration { get; set; } = Settings.DurationOfExercise;

        public bool IsNotPurchased { get; set; }

        public ObservableCollection<ExerciseProgress> ProgressData { get; set; } = new ObservableCollection<ExerciseProgress>();


        public WorkoutPage()
        {
            InitializeComponent();

            BindingContext = this;

            _excercises = _excercises.Shuffle();

            _audioService = DependencyService.Get<IAudio>();

            WorkoutDaysLabel = Settings.WorkoutDays;

            _startWorkoutGestureRecognizer = new TapGestureRecognizer();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _connection = DependencyService.Get<ISQLiteDb>().GetConnection();
            _connection.CreateTable<ExerciseProgress>();

            ExerciseDuration = Settings.DurationOfExercise;

            IsNotPurchased = !Settings.IsPurchased;

            _startWorkoutGestureRecognizer.Tapped += StartWorkout_Tapped;
            startWorkout.GestureRecognizers.Add(_startWorkoutGestureRecognizer);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();

            _startWorkoutGestureRecognizer.Tapped -= StartWorkout_Tapped;
            startWorkout.GestureRecognizers.Remove(_startWorkoutGestureRecognizer);
        }

        private async void Settings_Tapped(object sender, EventArgs e)
        {
            await (sender as Image).TapEffect();

            await Navigation.PushAsync(new SettingsPage());
        }

        private async void StartWorkout_Tapped(object sender, EventArgs e)
        {
            startWorkout.BorderThickness = 10;

            await Task.Delay(500);

            WorkoutNotStarted = false;


            StartWorkout();


            startWorkout.BorderThickness = 0;
        }

        private void StartWorkout()
        {
            if (_exerciseIndex >= 4) {
                WorkoutCompleted();

                return;
            }

            ExerciseText = $"Exercise {_exerciseIndex + 1}/{_excercises.Count}";

            UpdateExerciseGif($"NeckRelieve.Exercises.ex_{_excercises[_exerciseIndex++]}.gif");

            if (Settings.Sound)
                _audioService.PlayBeep();

            StartTimer();
        }

        private void StartTimer()
        {
            _isTimerShouldRun = true;

            Progress = 0;

            Device.StartTimer(TimeSpan.FromSeconds(1), () => {
                if (!_isTimerShouldRun)
                    return false;

                if (Progress >= ExerciseDuration) {
                    StartWorkout();

                    return false;
                }

                Progress++;

                return true;
            });
        }


        private void UpdateExerciseGif(string exName)
        {
            gifContainer.Content = new GifImageViewControl {
                WidthRequest = 256,
                HeightRequest = 256,
                Source = ImageSource.FromResource(exName)
            };
        }

        private async void Restart_Clicked(object sender, EventArgs e)
        {
            await ResetWorkout();

            StartWorkout();
        }

        private async void Done_Clicked(object sender, EventArgs e)
        {
            await ResetWorkout();

            WorkoutNotStarted = true;
        }

        private async Task ResetWorkout()
        {
            _exerciseIndex = 0;
            _isTimerShouldRun = false;

            await Task.Delay(1100);

            ExcercisesNotDone = true;
        }

        private void WorkoutCompleted()
        {
            ExcercisesNotDone = false;

            if (Settings.Sound)
                _audioService.PlayApplause();

            UpdateWorkoutDays();

            WorkoutDaysLabel = Settings.WorkoutDays;
            CompleteText = $"Your {WorkoutDaysLabel} day. Keep up the good work.";

            Settings.PreviousDate = DateTime.Today;

            UpdateProgressChart();
        }

        private void UpdateProgressChart()
        {
            var exerciseProgress = _connection.Table<ExerciseProgress>().Where(e => e.Date == DateTime.Today).FirstOrDefault();
            if (exerciseProgress == null) {
                exerciseProgress = new ExerciseProgress { Date = DateTime.Today, Count = 1 };
            } else {
                exerciseProgress.Count += 1;
            }

            _connection.InsertOrReplace(exerciseProgress);

            ProgressData.Clear();

            var dataPoints = _connection.Table<ExerciseProgress>().OrderBy(s => s.Date).ToList();
            foreach (var item in dataPoints)
                ProgressData.Add(item);


            var max = Device.Idiom == TargetIdiom.Phone ? 7.0 : 14.0;

            var factor = max / (double)ProgressData.Count;
            if (factor > 1.0)
                factor = 1.0;
            else if (factor < .1)
                factor = .1;

            chart.PrimaryAxis.ZoomFactor = factor;
        }

        private void UpdateWorkoutDays()
        {
            if (Settings.WorkoutDays == 0) {
                Settings.WorkoutDays = 1;
            } else {
                var daysDifference = DateTime.Today - Settings.PreviousDate;

                if (daysDifference.Days > 1) {
                    Settings.WorkoutDays = 1;
                } else if (daysDifference.Days == 1) {
                    Settings.WorkoutDays = Settings.WorkoutDays + 1;
                }
            }
        }
    }
}
