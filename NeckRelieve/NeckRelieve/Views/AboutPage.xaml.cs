﻿using System.IO;
using System.Reflection;
using Xamarin.Forms;

namespace NeckRelieve.Views
{
    public partial class AboutPage : ContentPage
    {
        public AboutPage()
        {
            InitializeComponent();

            var webViewSource = new HtmlWebViewSource {
                Html = GetAboutHtml()
            };

            webView.Source = webViewSource;
        }

        private string GetAboutHtml()
        {
            string text = "";

            var assembly = typeof(AboutPage).GetTypeInfo().Assembly;

            using (var stream = assembly.GetManifestResourceStream($"NeckRelieve.About.about.html")) {
                using (var reader = new StreamReader(stream)) {
                    text = reader.ReadToEnd();
                }
            }

            return text;
        }
    }
}
