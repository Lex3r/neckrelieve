﻿using System;
using System.Linq;
using System.Collections.Generic;


namespace NeckRelieve.Extensions
{
    public static class EnumerableExtensions
    {
        public static List<T> Shuffle<T>(this List<T> enumerable)
        {
            var r = new Random();
            return enumerable.OrderBy(x => r.Next()).ToList();
        }
    }
}
