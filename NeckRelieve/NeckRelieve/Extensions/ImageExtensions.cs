﻿using System.Threading.Tasks;
using Xamarin.Forms;

namespace NeckRelieve.Extensions
{
    public static class ImageExtensions
    {
        public static async Task TapEffect(this Image image)
        {
            await image.ScaleTo(0.9, 50, Easing.Linear);
            await Task.Delay(100);
            await image.ScaleTo(1, 50, Easing.Linear);
        }
    }
}
